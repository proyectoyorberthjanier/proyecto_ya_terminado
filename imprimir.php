<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TODOTELOCOSO</title>

    <link rel="stylesheet" href="style.css">
    <a href="index.html"> <img align="left" class="imagen" src="img/logo01.png" id="imagen1"></a>
  
    
    <link rel="shortcut icon" href="/icono.ico" type="image/x-icon">

    <link rel="stylesheet" href="css/bootstrap.css">

    <ul class="nav nav-pills"  id="menu">
        <li class="nav-item">
          <a class="nav-link" href="index.html">Inicio</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="registro.html">Solicitud de Ventas</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="imprimir.php">Consultar</a>
        </li>
    </ul>
</head>
<body>

<br>

<section class="form-register1">
        <h4>Lista De Las Solicitudes De Ventas</h4>
    </section>
  <br><br>

        <div class="table-responsive">
      <table border="3" align="center">
       
        <tbody>
          <tr align="center">
            <td>Id</td>
            <td>Nro Solicitud</td>
            <td>Fecha Solicitud</td>
            <td>Rif</td>
            <td>Fecha Entrega</td>
            <td>Prenda</td>
            <td>Cantidad</td>
            <td>Codigo</td>
            <td>Cuello</td>
            <td>Manga</td>
            <td>Puño</td>
            <td>Bolsillo</td>
            <td>Cierre</td>
            <td>Botones</td>
            <td>Estampado</td>
            <td>Bordado</td>
            <td>Tela</td>
            <td>Color</td>
            <td colspan="2">Operaciones</td>
           
          </tr>
                <?php
                    include("conexion.php");
                    $query="SELECT * FROM prendas inner join datos WHERE datos.id=prendas.id";
                    $resultado= $conexion->query($query);
                    while($row=$resultado->fetch_assoc()){
                 ?> 
                 <tr align="center">
            <td><?php echo $row['id']; ?></td>         
            <td><?php echo $row['numero_solicitud']; ?></td>
            <td><?php echo $row['fecha_solicitud']; ?></td>
            <td><?php echo $row['rif']; ?></td>
            <td><?php echo $row['fecha_entrega']; ?></td>
            <td><?php echo $row['prenda_tipo']; ?></td>
            <td><?php echo $row['cantidad_prendas']; ?></td>
            <td><?php echo $row['codigo']; ?></td>
            <td><?php echo $row['tipo_cuello']; ?></td>
            <td><?php echo $row['tipo_manga']; ?></td>
            <td><?php echo $row['tipo_puno']; ?></td>
            <td><?php echo $row['tipo_bolsillo']; ?></td>
            <td><?php echo $row['tipo_cierre']; ?></td>
            <td><?php echo $row['tipo_botones']; ?></td>
            <td><?php echo $row['tipo_estampado']; ?></td>
            <td><?php echo $row['tipo_bordado']; ?></td>
            <td><?php echo $row['tipo_tela']; ?></td>
            <td><?php echo $row['color']; ?></td>
            <td><a href="modificar.php?id=<?php echo $row['id']; ?>">Modificar</a></td>
            <td><a href="eliminar.php?id=<?php echo $row['id']; ?>">Eliminar</a></td>
            
          </tr>
                 
                <?php
                    }
                ?> 
        </tbody>
      </table>
      </div>  
      
      
</body>
</html>