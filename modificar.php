<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SOLICITUS DE VENTAS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <a href="index.html"> <img align="left" src="img/logo01.png" id="imagen"></a>

    <link rel="stylesheet" href="style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <ul class="nav nav-pills" id="menu1">
        <li class="nav-item">
          <a class="nav-link" href="index.html">Inicio</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="registro.html">Solicitud de Ventas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="imprimir.php">Consultar</a>
          </li>
    </ul>
</head>

<body>
        

    <section class="form-register1">
        <h4>Registro De Informacion De La Solicitud De Venta</h4>
    </section>
  
    
	<div class="modal-dialog text-center">
        
            <div class="col-sm-12 seccion-principal">
                <div class="modal-content">

                         <?php
                            $id=$_REQUEST['id'];
                            include("conexion.php");
                            $query="SELECT * FROM prendas INNER JOIN datos WHERE prendas.id='$id' && datos.id='$id'";
                            $resultado= $conexion->query($query);
                            $row=$resultado->fetch_assoc();
                        ?> 
                        
                    <form action="modificar_proceso.php?id=<?php echo $row['id']; ?>" method="POST" name="form">
                        
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="">Nro de Solicitud</label>
                                <input class="form-control" required type="text" name="nro_solicitud"
                                    placeholder="N° de Solicitud" maxlength="15" value="<?php echo $row['numero_solicitud'];?>">
                            </div>
                           
                            
                            <div class="form-group col-6">
                                <label for="">Fecha de Solicitud</label>
                                <br><input class="form-control" required type="date" max="2000-12-31" min="1950-01-01"
                                    name="fecha_solicitud" value="<?php echo $row['fecha_solicitud'];?>">
                            </div>

                            <div class="form-group col-6">
                                <label for="">RIF Asociado</label>
                                <input class="form-control" required type="text" name="rif"
                                    placeholder="Rif Asociado" maxlength="15" value="<?php echo $row['rif'];?>">
                            </div>

                            <div class="form-group col-6">
                                <label for="">Fecha de Entrega</label>
                                <input class="form-control" required type="date" max="2000-12-31" min="1950-01-01"
                                    name="fecha_entrega" value="<?php echo $row['fecha_entrega'];?>">
                            </div>

                            <div class="form-group col-6">
                                <select class="form-control" class="input" name="tipodeprenda" value="<?php echo $row['tipodeprenda'];?>">
                                    <option value="1" selected>Uniformes</option>
                                    <option value="2">Camisas</option>
                                    <option value="3">Pantalones</option>
                                    <option value="4">Chaquetas</option>
                                    <option value="5">Fluxes</option>
                                </select>
                            </div>
                           
                            <div class="form-group col-6">
                                <input class="form-control" required type="number" name="cantidad" placeholder="Cantidad de Prendas"
                                    maxlength="8" value="<?php echo $row['cantidad_prendas'];?>">
                            </div>
                            <div class="form-group col-12">
                                <input class="form-control" required type="number" name="coigo"
                                  placeholder="Cogigo del Modelo" maxlength="20" value="<?php echo $row['codigo'];?>">
                            </div>

                            
                                <div class="form-group col-4">
                                    <input class="form-control" required type="text" name="tipo_cuello"
                                        placeholder="Tipo de cuello" maxlength="15" value="<?php echo $row['tipo_cuello'];?>">
                                </div>

                                <div class="form-group col-4">
                                    <input class="form-control" required type="text" name="tipo_manga"
                                        placeholder="Tipo de manga" maxlength="15" value="<?php echo $row['tipo_manga'];?>">
                                </div>

                                <div class="form-group col-4">
                                    <input class="form-control" required type="text" name="tipo_puno"
                                        placeholder="Tipo de puño" maxlength="15" value="<?php echo $row['tipo_puno'];?>">
                                </div>

                                <div class="form-group col-4">
                                    <input class="form-control" required type="text" name="tipo_bolsillo"
                                        placeholder="Tipo de bolsillo" maxlength="15" value="<?php echo $row['tipo_bolsillo'];?>">
                                </div>

                                <div class="form-group col-4">
                                    <input class="form-control" required type="text" name="tipo_cierre"
                                        placeholder="Tipo de cierre" maxlength="15" value="<?php echo $row['tipo_cierre'];?>">
                                </div>

                                <div class="form-group col-4">
                                    <input class="form-control" required type="text" name="tipo_botones"
                                        placeholder="Tipo de botones" maxlength="15" value="<?php echo $row['tipo_botones'];?>">
                                </div>

                                <div class="form-group col-6">
                                    <input class="form-control" required type="text" name="tipo_estampado"
                                        placeholder="Tipo de estampado" maxlength="15" value="<?php echo $row['tipo_estampado'];?>">
                                </div>

                                <div class="form-group col-6">
                                    <input class="form-control" required type="text" name="tipo_bordado"
                                        placeholder="Tipo de bordado" maxlength="15" value="<?php echo $row['tipo_bordado'];?>">
                                </div>

                                <div class="form-group col-6">
                                    <input class="form-control" required type="text" name="tipo_tela"
                                        placeholder="Tipo de tela" maxlength="15" value="<?php echo $row['tipo_tela'];?>">
                                </div>

                                <div class="form-group col-6">
                                    <input class="form-control" required type="text" name="color"
                                        placeholder="color" maxlength="15" value="<?php echo $row['color'];?>">
                                </div>
                            
                            

                        <div class="form-group col-12">
                                <button class="btn btn-danger" type="reset">Borrar</button>
                                <button class="btn btn-success" type="submit">Aceptar</button>
                                
                        </div>
                       
                    </form>
                    
                    
                </div>
                   
            </div>
            
</div>
        
   
    
					
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.js"></script>
</body>
</html>